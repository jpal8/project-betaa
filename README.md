# CarCar

Team:

Celeste Villar - Sales microservice implementations and front-end for VehicleModelForm, AutomobileList, and AutomobileForm

Jessica Palacios - Services microservice implementations and front-end for ManufacturerList, ManufacturerForm, and VehichleModelList

## Design
This a a automobile sales and service application. The design of our system includes the use of Automobile Value Object (AVO) to poll data from the Inventory microservice to the Sales and Service microservices. The inventory microservice allows for the creation of manufacturers, models, and vehicles, each with a unique identifier in the form of a VIN number.

## Service microservice
The Service microservice is designed to create a backend for appointment management, including the ability to create, list, and filter appointments by VIN in the service history list. Additionally, the microservice allows for the creation of technicians, which can be assigned to appointments as a foreign key. By utilizing the AVO from the inventory microservice, the application allows for the addition of appointments to the database, with the associated AVO. The appointment model collects information such as the customer's name, VIN number, date and time, assigned technician, and reason for the appointment. The technician model collects the technician's name and assigns them a unique employee id number.


## Sales microservice
The goal for the sales microservice is to track customer, sales represetative, and automobile sales data.
By polling automobiles from the inventory microservice, the application allows you to add a sale to the database with automobiles from the inventory.
For our customer model we collect the customers name, address, and phone number.
For our sales person model we collect the employee's name and create an employee number that's unique to them.
For our sales model it is made up of the sales person, the customers name, the listing price of the automobile, and the vin number of the specific automobile.

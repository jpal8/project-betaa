import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import TechnicianForm from './Services/TechnicianForm'
import AppointmentList from './Services/AppointmentList'
import AppointmentForm from './Services/AppointmentForm'
import ServiceHistory from './Services/ServiceHistory'

import ManufacturerList from './Inventory/ManufacturerList'
import ManufacturerForm from './Inventory/ManufacturerForm'
import VehichleModelList from './Inventory/VehicleModelList'
import VehicleModelForm from './Inventory/VehicleModelForm'
import AutomobileList from './Inventory/AutomobileList'
import AutomobileForm from './Inventory/AutomobileForm'

import CustomerForm from './Sales/CustomerForm'
import SalePersonForm from './Sales/SalePersonForm'
import SaleHistoryList from './Sales/SaleHistoryList'
import SaleForm from './Sales/SaleForm'
import SalesList from './Sales/SalesList'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technician">
            <Route path="" element={<TechnicianForm />} />
          </Route>
          <Route path="appointment">
            <Route path="" element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<VehichleModelList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="/customers">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="/salespeople">
            <Route path="new" element={<SalePersonForm />} />
          </Route>
          <Route path="/autosales">
            <Route path="" element={<SalesList />} />
            <Route path="new" element={<SaleForm />} />
            <Route path="history" element={<SaleHistoryList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter >
  );
}

export default App;

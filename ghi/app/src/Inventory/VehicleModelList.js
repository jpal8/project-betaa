import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

function VehicleModelList() {
    const [models, setModels] = useState([])
    useEffect(() => {
        async function fetchData() {
            const url = 'http://localhost:8100/api/models/'
            const response = await fetch(url)
            if (response.ok) {
                let data = await response.json()
                setModels(data.models)
            }
        }
        fetchData()
    }, [])

    return (
        <div className='container pt-5'>
            <h1>Vehicle Models</h1>
            <div className='d-grid gap-2 d-sm-flex justify-content-sm-right'>
                <Link to='/models/new/' className='btn btn-outline-success px-4 gap-3 mb-4 mt-3'>Add a New Vehicle Model</Link>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Name</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.manufacturer.name}</td>
                                <td>{model.name}</td>
                                <td>
                                    <img style={{ height: '100px', width: '200px' }} src={model.picture_url} alt='' />
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default VehicleModelList

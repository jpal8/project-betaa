import React, { useState, useEffect } from 'react';

function SaleHistoryList() {
    const [list, setList] = useState();

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/autosales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setList(data.autosales)
        }
    }

    const [query, setQuery] = useState('')

    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div>
            <div className="container">
                <div className="row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                        <div className="form-group">
                            <div className="input-group">
                                <input onChange={event => setQuery(event.target.value)} className="form-control" placeholder="Search a Sales Person" type="text" />
                            </div>
                        </div>
                    </form>
                </div>
                <h1>Sales History </h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Rep</th>
                            <th>Employee ID</th>
                            <th>Customer Name</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list?.filter(autosale =>
                            autosale.sales_person.sales_person.includes(query))
                            .map(autosale => {
                                return (
                                    <tr key={autosale.id}>
                                        <td>{autosale.sales_person.sales_person}</td>
                                        <td>{autosale.sales_person.employee_number}</td>
                                        <td>{autosale.customer.customer}</td>
                                        <td>{autosale.automobile.vin}</td>
                                        <td>{"$" + autosale.price}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SaleHistoryList;

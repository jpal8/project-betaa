import { useState, useEffect } from 'react';

function SalesList() {
    const [autosales, setAutosales] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/autosales/')
        const data = await response.json()
        setAutosales(data.autosales)
    }
    useEffect(() => {
        getData();
    }, []
    )

    return (
        <div>
            <h1>Sales List</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Customer</th>
                        <th>Price</th>
                        <th>VIN</th>
                    </tr>
                </thead>
                <tbody>
                    {autosales.map(autosale => {
                        return (
                            <tr key={autosale.id}>
                                <td>{autosale.sales_person.sales_person}</td>
                                <td>{autosale.sales_person.employee_number}</td>
                                <td>{autosale.customer.customer}</td>
                                <td>{"$" + autosale.price}</td>
                                <td>{autosale.automobile.vin}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesList;

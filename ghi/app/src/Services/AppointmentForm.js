import React, { useState, useEffect } from 'react'

const AppointmentForm = () => {
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [technician, setTechnician] = useState('')
    const [technicians, setTechnicians] = useState([])
    const [reason, setReason] = useState('')
    const [success, setSuccess] = useState('alert alert-success d-none')

    const handleChange = (event) => {
        const value = event.target.value
        switch (event.target.name) {
            case 'vin':
                setVin(value)
                break
            case 'customer':
                setCustomer(value)
                break
            case 'date':
                setDate(value)
                break
            case 'time':
                setTime(value)
                break
            case 'technician':
                setTechnician(value)
                break
            case 'reason':
                setReason(value)
                break
            default:
                break
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = { vin, customer, date, time, technician, reason }
        const appointmentUrl = 'http://localhost:8080/api/appointment/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            const newAppointment = await response.json()
            setVin('')
            setCustomer('')
            setDate('')
            setTime('')
            setTechnician('')
            setReason('')
            setSuccess('alert alert-success')
        }
    }

    const componentDidMount = async () => {
        const url = 'http://localhost:8080/api/technician/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technician)
        }
    }

    useEffect(() => {
        componentDidMount()
    }, [])

    const timer = () => {
        setTimeout(() => {
            setSuccess('alert alert-success d-none')
        }, 400)
    }

    return (

        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>New Appointment</h1>
                    <form onSubmit={handleSubmit} id='create-location-form'>
                        <div className='form-floating mb-3'>
                            <input value={vin} onChange={handleChange} placeholder='VIN' required type='int' name='vin' id='vin' className='form-control' />
                            <label htmlFor='vin'>VIN</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={customer} onChange={handleChange} placeholder='Name' required type='text' name='customer' id='customer' className='form-control' />
                            <label htmlFor='customer'>Customer Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={date} onChange={handleChange} placeholder='Date' required type='date' name='date' id='date' className='form-control' />
                            <label htmlFor='date'>Date</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={time} onChange={handleChange} placeholder='Time' required type='time' name='time' id='time' className='form-control' />
                            <label htmlFor='time'>Time</label>
                        </div>
                        <div className='mb-3'>
                            <select value={technician} onChange={handleChange} placeholder='Technician' required type='text' name='technician' id='technician' className='form-control'>
                                <option value=''>Technician</option>
                                {technicians?.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>
                                            {technician.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className='form-floating mb-3'>
                            <textarea value={reason} onChange={handleChange} placeholder='Reason' required type='text' name='reason' id='reason' className='form-control'></textarea>
                            <label htmlFor='reason'>Reason</label>
                        </div>
                        <div className='form-group text-center'>
                            <input type='submit' className='btn btn-primary' value='Save' />
                        </div>
                    </form>
                    <div className={success}>Appointment created successfully</div>
                </div>
            </div>
        </div>
    )
}

export default AppointmentForm

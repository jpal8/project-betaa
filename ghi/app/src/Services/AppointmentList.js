import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

const AppointmentList = () => {
    const [appointments, setAppointments] = useState([])

    const finishedAppointment = async (id) => {
        const appointmentUrl = `http://localhost:8080/api/appointment/${id}/`

        const fetchConfig = {
            method: 'PUT',
            body: JSON.stringify({ finished: true }),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(appointmentUrl, fetchConfig)

        if (response.ok) {
            const finished = await response.json()
            appointmentsListed()
        }
    }

    const cancelAppointment = async (id) => {
        const appointmentUrl = `http://localhost:8080/api/appointment/${id}/`

        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(appointmentUrl, fetchConfig)

        if (response.ok) {
            const deleted = await response.json()
            appointmentsListed()
        }
    }

    const [automobiles, setAutomobiles] = useState([])
    const fetchAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json()
            const vinList = []
            data.autos.map((automobile) => vinList.push(automobile.vin))
            setAutomobiles(vinList)
        }
    }

    useEffect(() => {
        fetchAutomobiles()
    }, [])

    const vip = (vin) => {
        if (automobiles.includes(vin)) {
            return 'YES'
        } else {
            return 'NO'
        }
    }

    const appointmentsListed = async () => {
        const response = await fetch('http://localhost:8080/api/appointment/')
        if (response.ok) {
            const data = await response.json()
            const filterAppointments = data.appointments.filter(appointment => appointment.finished === false)
            setAppointments(filterAppointments)
        }
        else {
            console.error(response)
        }
    }

    useEffect(() => {
        appointmentsListed()
    }, [])

    let appointmentHistory
    if (appointments.length === 0) {
        appointmentHistory = <p>No appointments were found.</p>
    } else {
        appointmentHistory =
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td><strong>{vip(appointment.vin)}</strong></td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.technician.name}</td>
                                {<td>
                                    <button className='btn btn-danger' onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
                                </td>}
                                {<td>
                                    <button className='btn btn-success' onClick={() => finishedAppointment(appointment.id)}>Finished</button>
                                </td>}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
    }
    return (
        <div className='container pt-5'>
            <h1>Service Appointments</h1>
            <div className='d-grid gap-2 d-sm-flex justify-content-sm-end'>
                <Link to='/appointment/new' className='btn btn-outline-success btn-lg px-4 gap-3'>Schedule new appointment</Link>
            </div>
            {appointmentHistory}
        </div>
    )
}

export default AppointmentList

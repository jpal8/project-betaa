import React, { useState } from 'react'

const TechnicianForm = () => {
    const [name, setName] = useState('')
    const [id, setId] = useState('')
    const [success, setSuccess] = useState(false)
    const [error, setError] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = { name, id }
        const techniciansUrl = 'http://localhost:8080/api/technician/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(techniciansUrl, fetchConfig)
        if (response.ok) {
            const newTechnician = await response.json()
            setName('')
            setId('')
            setSuccess(true)
        } else {
            setError(true)
        }
    }

    const handleInputChange = (event) => {
        const { name, value } = event.target
        if (name === 'name') {
            setName(value)
        } else {
            setId(value)
        }
    }

    return (
        <div className='container pt-5'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add Technician</h1>
                    <form onSubmit={handleSubmit} id='create-technician-form'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleInputChange} value={name} placeholder='Name' required type='text' name='name' id='name' className='form-control' />
                            <label htmlFor='name'>Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleInputChange} value={id} placeholder='Employee ID<' required type='number' name='id' id='id' className='form-control' />
                            <label htmlFor='id'>Employee ID</label>
                        </div>
                        <button className='btn btn-success'>Add</button>
                    </form>
                    <div className={`alert alert-success mt-5 ${success ? '' : 'd-none'}`}>
                        Successfully added a new technician!
                    </div>
                    <div className={`alert alert-danger mt-5 ${error ? '' : 'd-none'}`}>
                        That Employee ID is already being used! Assign a different Employee ID number!
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm

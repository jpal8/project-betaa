from django.contrib import admin
from .models import AutomobileVO, Customer, Salesperson, Autosale


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Customer)
class TechnicianAdmin(admin.ModelAdmin):
    pass


@admin.register(Salesperson)
class AppointmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Autosale)
class AppointmentAdmin(admin.ModelAdmin):
    pass

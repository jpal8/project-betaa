# Generated by Django 4.0.3 on 2023-01-24 23:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_alter_technician_employee_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appointment',
            old_name='technichian',
            new_name='technician',
        ),
    ]

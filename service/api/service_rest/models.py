from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True, null=True)

    def __str__(self):
        return f"{self.name}, {self.employee_id}"

    def get_api_url(self):
        return reverse("list_technician", kwargs={"pk": self.pk})


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    reason = models.CharField(max_length=100)
    finished = models.BooleanField(default=False)
    canceled = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("list_appointment", kwargs={"pk": self.pk})
